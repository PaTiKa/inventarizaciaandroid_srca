package ge.itforce.pkoridze.inventarizacia;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintAttributes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.R.layout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AxaliChanacerisSheqmna extends AppCompatActivity {
    String sackisi_id="";

    Spinner Spinner_ganzomilebisarcheva;
    ArrayAdapter<CharSequence> Adapter_ganzomilebebisSia;
    String unikaluriID;
    String[] Suratebi=new String[]{"","","","","","","","","",""};
    PTK_row row=null;
    PTK_row row_sackisi=null;

    int Current_Calendar_editori=0;

    int BARCODE_SCANNER =2;
    int CAPTURE_IMAGE= 0;
    PTK_row sheavse=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_axali_chanaceris_sheqmna);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });



        try {
            final RelativeLayout rl = (RelativeLayout) findViewById(R.id.AxaliChanaceriLayout);
            final ViewTreeObserver vto = rl.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    rl.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    if (sheavse != null)
                        setInitialData(sheavse);

                }
            });
        }
        catch (Exception ex) {

            ex.printStackTrace();
        }


//todo: motrialebisas infos kargavs
        if (savedInstanceState != null)
        {
            sackisi_id = (String) savedInstanceState.getSerializable("sackisi_id");

            unikaluriID = savedInstanceState.getString("unikaluriID"); // savedInstanceState.getParcelable("imageCaptureUri");
            Current_Calendar_editori = savedInstanceState.getInt("Current_Calendar_editori");
            Suratebi = savedInstanceState.getStringArray("suratebis_sia");

            try {
                ((EditText) findViewById(R.id.Value_DamzadebisTarigi)).setText(savedInstanceState.getString("DamzadebisTarigi"));
                ((EditText) findViewById(R.id.Value_GamokenebisVada)).setText(savedInstanceState.getString("GamokenebisVada"));
                ((EditText) findViewById(R.id.Value_MigebisTarigi)).setText(savedInstanceState.getString("MigebisTarigi"));
                ((EditText) findViewById(R.id.Value_QRcode)).setText(savedInstanceState.getString("QRcode"));
                ((EditText) findViewById(R.id.Value_Info)).setText(savedInstanceState.getString("Info"));
                ((EditText) findViewById(R.id.Value_Raodenoba)).setText(savedInstanceState.getString("Raodenoba"));
                ((EditText) findViewById(R.id.Value_Dasaxeleba)).setText(savedInstanceState.getString("Dasaxeleba"));
                ((EditText) findViewById(R.id.Value_Descr)).setText(savedInstanceState.getString("Descr"));
                setSpinnerSelectedItemByValue((Spinner) findViewById(R.id.Value_Ganzomileba), savedInstanceState.getString("Ganzomileba"));
            }
            catch (Exception e){
                e.printStackTrace();
            }


            try {
                if (Suratebi.length > 0) setPhoto(R.id.PTK_foto_0, Suratebi[0]);
                if (Suratebi.length > 1) setPhoto(R.id.PTK_foto_1, Suratebi[1]);
                if (Suratebi.length > 2) setPhoto(R.id.PTK_foto_2, Suratebi[2]);
                if (Suratebi.length > 3) setPhoto(R.id.PTK_foto_3, Suratebi[3]);
                if (Suratebi.length > 4) setPhoto(R.id.PTK_foto_4, Suratebi[4]);
                if (Suratebi.length > 5) setPhoto(R.id.PTK_foto_5, Suratebi[5]);
                if (Suratebi.length > 6) setPhoto(R.id.PTK_foto_6, Suratebi[6]);
                if (Suratebi.length > 7) setPhoto(R.id.PTK_foto_7, Suratebi[7]);
                if (Suratebi.length > 8) setPhoto(R.id.PTK_foto_8, Suratebi[8]);
                if (Suratebi.length > 9) setPhoto(R.id.PTK_foto_9, Suratebi[9]);


            } catch (Exception e){
                e.printStackTrace();
            }


        }
        else {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                sackisi_id= "";
            } else {

                sackisi_id= extras.getString("sackisi_id");
            }

            Long tsLong = System.currentTimeMillis();
            unikaluriID = tsLong.toString();

            if(sackisi_id!=null && sackisi_id!="") {
                unikaluriID = sackisi_id;
                PTK_DatabaseHandler db =  new PTK_DatabaseHandler(this);
                 sheavse = db.getChanaceri(sackisi_id);
                row_sackisi = sheavse;

            }
        }
/*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        PTKinit();
        /*if(sheavse!=null)
            setInitialData(sheavse);*/


    }

    public void setInitialData(PTK_row row) {
        this.row = row;
        unikaluriID=row._Id;

        Suratebi = row.Suratebi;

        try {
            ((EditText) findViewById(R.id.Value_DamzadebisTarigi)).setText(row.getDamzadebisTarigi());
            ((EditText) findViewById(R.id.Value_GamokenebisVada)).setText(row.getGamokenebisVada());
            ((EditText) findViewById(R.id.Value_MigebisTarigi)).setText(row.getMigebisTarigi());
            ((EditText) findViewById(R.id.Value_QRcode)).setText(row.getQRcode());
            ((EditText) findViewById(R.id.Value_Info)).setText(row.getInfo());
            ((EditText) findViewById(R.id.Value_Raodenoba)).setText(row.getRaodenoba());
            ((EditText) findViewById(R.id.Value_Dasaxeleba)).setText(row.getName());
            ((EditText) findViewById(R.id.Value_Descr)).setText(row.getDescription());
            setSpinnerSelectedItemByValue((Spinner) findViewById(R.id.Value_Ganzomileba), row.getGanzomilebaID());
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            String[] surz=row.getSuratebi();
            if(surz!=null && surz.length>0) {
                if (surz.length > 0 ) setPhoto(R.id.PTK_foto_0,surz[0]);
                if (surz.length > 1 ) setPhoto(R.id.PTK_foto_1,surz[1]);
                if (surz.length > 2 ) setPhoto(R.id.PTK_foto_2,surz[2]);
                if (surz.length > 3 ) setPhoto(R.id.PTK_foto_3,surz[3]);
                if (surz.length > 4 ) setPhoto(R.id.PTK_foto_4,surz[4]);
                if (surz.length > 5 ) setPhoto(R.id.PTK_foto_5,surz[5]);
                if (surz.length > 6 ) setPhoto(R.id.PTK_foto_6,surz[6]);
                if (surz.length > 7 ) setPhoto(R.id.PTK_foto_7,surz[7]);
                if (surz.length > 8 ) setPhoto(R.id.PTK_foto_8,surz[8]);
                if (surz.length > 9 ) setPhoto(R.id.PTK_foto_9,surz[9]);
               /* if (surz.length > 0 ) setPhoto(R.id.PTK_foto_0,surz[0]);
                    ((ImageView) findViewById(R.id.PTK_foto_0)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[0])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_0)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 1 && surz[1]!=null &&  surz[1]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_1)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[1])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_1)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 2&& surz[2]!=null && surz[2]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_2)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[2])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_2)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 3&& surz[3]!=null && surz[3]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_3)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[3])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_3)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 4&& surz[4]!=null && surz[4]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_4)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[4])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_4)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 5&& surz[5]!=null && surz[5]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_5)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[5])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_5)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 6&& surz[6]!=null && surz[6]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_6)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[6])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_6)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 7&& surz[7]!=null && surz[7]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_7)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[7])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_7)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 8&& surz[8]!=null && surz[8]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_8)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[8])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_8)).setImageResource(R.mipmap.ptk_photocamera);
                }
                if (surz.length > 9&& surz[9]!=null && surz[9]!="")
                    ((ImageView) findViewById(R.id.PTK_foto_9)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+surz[9])));
                else {
                    ((ImageView) findViewById(R.id.PTK_foto_9)).setImageResource(R.mipmap.ptk_photocamera);
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPhoto(int resource_id, String fname) {
        boolean ok=false;
        if(fname!=null && fname!="" && fname!="null") {
            File tmpfile =new File("" + Environment.getExternalStorageDirectory() + File.separator + "PTK" + File.separator + fname);
            if(tmpfile.getName().endsWith(".jpg") && tmpfile.exists()) {
                ((ImageView) findViewById(resource_id)).setImageURI(Uri.fromFile(tmpfile));
                ok = true;
            }
        }
        if(!ok || fname==null)
            ((ImageView) findViewById(resource_id)).setImageResource(R.mipmap.ptk_photocamera);
    }

    public void setSpinnerSelectedItemByValue(Spinner spinner, String compareValue){
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this, R.array.Ganzomilebebi, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (!compareValue.equals(null)) {
            int spinnerPosition = adapter.getPosition(compareValue);
            spinner.setSelection(spinnerPosition);
        }
    }


    public void handleUncaughtException (Thread thread, Throwable e)
    {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        return;
        /*Intent intent = new Intent ();
        intent.setAction ("com.mydomain.SEND_LOG"); // see step 5.
        intent.setFlags (Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
        startActivity (intent);

        System.exit(1); // kill off the crashed app*/
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putString("unikaluriID", unikaluriID);//putParcelable("imageCaptureUri", mImageCaptureUri);
        outState.putInt("Current_Calendar_editori", Current_Calendar_editori);//putParcelable("imageCaptureUri", mImageCaptureUri);
        outState.putStringArray("suratebis_sia", Suratebi);

        try {
            outState.putString("DamzadebisTarigi", ((EditText) findViewById(R.id.Value_DamzadebisTarigi)).getText().toString());
            outState.putString("GamokenebisVada", ((EditText) findViewById(R.id.Value_GamokenebisVada)).getText().toString());
            outState.putString("MigebisTarigi", ((EditText) findViewById(R.id.Value_MigebisTarigi)).getText().toString());
            outState.putString("QRcode", ((EditText) findViewById(R.id.Value_QRcode)).getText().toString());
            outState.putString("Info", ((EditText) findViewById(R.id.Value_Info)).getText().toString());
            outState.putString("Raodenoba", ((EditText) findViewById(R.id.Value_Raodenoba)).getText().toString());
            outState.putString("Descr", ((EditText) findViewById(R.id.Value_Descr)).getText().toString());
            outState.putString("Dasaxeleba", ((EditText) findViewById(R.id.Value_Dasaxeleba)).getText().toString());
            outState.putString("Ganzomileba", ((Spinner) findViewById(R.id.Value_Ganzomileba)).getSelectedItem().toString());

        }catch (Exception e) {
            e.printStackTrace();
        }
        try{
        for(int i=0;i<10;i++) {
            outState.putString("surati_" + (i + 1), Suratebi[i]);
        }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void PTKinit(){

        Suratebi[0]="";
        Suratebi[1]="";
        Suratebi[2]="";
        Suratebi[3]="";
        Suratebi[4]="";
        Suratebi[5]="";
        Suratebi[6]="";
        Suratebi[7]="";
        Suratebi[8]="";
        Suratebi[9]="";

        //PTK
        Spinner_ganzomilebisarcheva = (Spinner)findViewById(R.id.Value_Ganzomileba);
        Adapter_ganzomilebebisSia = ArrayAdapter.createFromResource(this, R.array.Ganzomilebebi, layout.simple_spinner_item);
        Adapter_ganzomilebebisSia.setDropDownViewResource(layout.simple_spinner_dropdown_item);
        Spinner_ganzomilebisarcheva.setAdapter(Adapter_ganzomilebebisSia);
        Spinner_ganzomilebisarcheva.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageButton btn = (ImageButton) findViewById(R.id.PTK_select_migebis_tarigi);
        btn.setVisibility(View.GONE);
        EditText et = (EditText) findViewById(R.id.Value_MigebisTarigi);
        et.setVisibility(View.GONE);

        btn = (ImageButton) findViewById(R.id.PTK_select_damzadebis_tarigi);
        btn.setVisibility(View.GONE);
        et = (EditText) findViewById(R.id.Value_DamzadebisTarigi);
        et.setVisibility(View.GONE);

        btn = (ImageButton) findViewById(R.id.PTK_select_GamokenebisVada);
        btn.setVisibility(View.GONE);
        et = (EditText) findViewById(R.id.Value_GamokenebisVada);
        et.setVisibility(View.GONE);






    }


    public void onClickCheckBoxi(View v) {
        CheckBox chk = (CheckBox)v;
        boolean chkedia=chk.isChecked();

        if(v.getId()==R.id.PTK_checkbox_MigebisTarigi) {

            ImageButton btn = (ImageButton) findViewById(R.id.PTK_select_migebis_tarigi);
            btn.setVisibility(chkedia?View.VISIBLE:View.GONE);
            EditText et = (EditText) findViewById(R.id.Value_MigebisTarigi);
            et.setVisibility(chkedia?View.VISIBLE:View.GONE);
        }
        else {
            if(v.getId()==R.id.PTK_checkbox_DamzadebisTarigi) {

                ImageButton btn = (ImageButton) findViewById(R.id.PTK_select_damzadebis_tarigi);
                btn.setVisibility(chkedia?View.VISIBLE:View.GONE);
                EditText et = (EditText) findViewById(R.id.Value_DamzadebisTarigi);
                et.setVisibility(chkedia?View.VISIBLE:View.GONE);
            }
            else {
                if(v.getId()==R.id.PTK_checkbox_GamokenebisVada) {

                    ImageButton btn = (ImageButton) findViewById(R.id.PTK_select_GamokenebisVada);
                    btn.setVisibility(chkedia?View.VISIBLE:View.GONE);
                    EditText et = (EditText) findViewById(R.id.Value_GamokenebisVada);
                    et.setVisibility(chkedia?View.VISIBLE:View.GONE);
                }
            }
        }

    }

    Calendar myCalendar = Calendar.getInstance();

    public  void  onClickSelect_migebis_tarigi(View v){
        EditText mimdinare=(EditText)findViewById(R.id.Value_MigebisTarigi);
        Current_Calendar_editori=1;
        SelectKalendridan(mimdinare);
    }

    public  void  onClickSelect_damzadebis_tarigi(View v){
        EditText mimdinare=(EditText)findViewById(R.id.Value_DamzadebisTarigi);
        Current_Calendar_editori=2;
        SelectKalendridan(mimdinare);
    }

    public  void  onClickSelect_GamokenebisVada(View v){
        EditText mimdinare=(EditText)findViewById(R.id.Value_GamokenebisVada);
        Current_Calendar_editori=3;
        SelectKalendridan(mimdinare);
    }

    private void SelectKalendridan(EditText mimdinare) {

        String tarigi=null;

        //mimdinare.requestFocus();


        if(mimdinare!=null){
            try {
                tarigi = mimdinare.getText().toString();
            }catch (Exception ex){

            }
        }
        if(tarigi!=null && tarigi!=""){
            String[] dtt=tarigi.split("/");
            if(dtt.length==3){
                try {
                    int dge = Integer.parseInt(dtt[0]);
                    int tve = Integer.parseInt(dtt[1]);
                    int tseli = Integer.parseInt(dtt[2]);
                    tve=tve>0?tve-1:tve;
                    myCalendar.set(tseli, tve, dge);
                }catch (Exception ex){

                }
            }
            /*DateFormat dt=new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date dtt = (Date)dt.parse(tarigi);
                myCalendar.setTime(dtt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            */

        }

        int Year=myCalendar.get(Calendar.YEAR);
        int Month=myCalendar.get(Calendar.MONTH);
        int Day=myCalendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(AxaliChanacerisSheqmna.this, date, Year, Month,Day).show();
    }

   /*
    public void onClickSelectCalendar(View v){
        String tarigi=null;
        EditText mimdinare=null;
        String tag=(String)v.getTag();

        switch(tag){
            case "migebis_tarigi":
                Current_Calendar_editori=1;

                mimdinare=(EditText)findViewById(R.id.Value_MigebisTarigi);
                break;
            case "damzadebis_tarigi":
                Current_Calendar_editori=1;

                mimdinare=(EditText)findViewById(R.id.Value_DamzadebisTarigi);
                break;
            case "GamokenebisVada":
                Current_Calendar_editori=1;

                mimdinare=(EditText)findViewById(R.id.Value_GamokenebisVada);
                break;
        }



        if(mimdinare!=null){
            try {
                tarigi = mimdinare.getText().toString();
            }catch (Exception ex){

            }
        }
        if(tarigi!=null){
            DateFormat dt=new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date dtt = (Date)dt.parse(tarigi);
                myCalendar.setTime(dtt);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        int Year=myCalendar.get(Calendar.YEAR);
        int Month=myCalendar.get(Calendar.MONTH);
        int Day=myCalendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(AxaliChanacerisSheqmna.this, date, Year, Month,Day).show();
    }
*/


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        EditText edittext=null;
        switch(Current_Calendar_editori) {
            case 1:
                edittext = (EditText) findViewById(R.id.Value_MigebisTarigi);
                break;
            case 2:
                edittext = (EditText) findViewById(R.id.Value_DamzadebisTarigi);
                break;
            case 3:
                edittext = (EditText) findViewById(R.id.Value_GamokenebisVada);
                break;
        }
        if(edittext!=null)
             edittext.setText(sdf.format(myCalendar.getTime()));

        Current_Calendar_editori=0;
    }

    //////////////////--------------------------->
    /////////////////////////// suratis gadagebis funqcia
    public void PhotosGadagebaOnClick(View v) {

        Intent myIntent = new Intent(this, FotosGadageba.class);
        myIntent.putExtra("v", (String)v.getTag());
        myIntent.putExtra("id", v.getId());
        myIntent.putExtra("unikaluriID", unikaluriID);
        myIntent.putExtra("fname", unikaluriID +"_"+(String)v.getTag()+".jpg");
        this.startActivityForResult(myIntent, CAPTURE_IMAGE);

    }


    /////////////////////////// suratis gadagebis funqcia
    //////////////////<----------------------------------

    //////////////////--------------------------->
    /////////////////////////// QR code scanirebis funqcia


    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    //product barcode mode
    public void scanBar(View v) {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            startActivityForResult(intent, BARCODE_SCANNER);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    //product qr code mode
    public void scanQR(View v) {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, BARCODE_SCANNER);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    //alert dialog for downloadDialog
    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }


    /////////////////////////// QR code scanirebis funqcia
    //////////////////<----------------------------------



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE) { /////////////////////////// suratis gadagebis funqcia
            if (resultCode == RESULT_OK) {

                if (data.hasExtra("PTK_pic") && data.hasExtra("id") && data.hasExtra("PTK_newImage") && data.hasExtra("PTK_delete")) {
                /*Toast.makeText(this, data.getExtras().getString("myData1"),
                        Toast.LENGTH_SHORT).show();*/
                    boolean PTK_newImage = data.getBooleanExtra("PTK_newImage", false);
                    boolean PTK_delete = data.getBooleanExtra("PTK_delete", false);
                    if (PTK_delete) {

                    } else {
                        if (PTK_newImage) {
                            int idi = data.getIntExtra("id", 0);
                            //Bitmap pic = data.getParcelableExtra("PTK_pic");
                            //ImageView iv = (ImageView) findViewById(idi);
                            //iv.setImageBitmap(pic);

                            String tag = data.getStringExtra("tag");

                            Uri pic = data.getParcelableExtra("PTK_pic");
                            //File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
                            //File from = new File(directory, "PTK_CAPTURED_IMAGE.jpg");
                            //File to = new File(directory, tag + ".jpg");

                            ImageView iv = (ImageView) findViewById(idi);
                            iv.setImageURI(pic);

                            try {
                                String url=pic.getPath();
                                url=url.substring(url.lastIndexOf('/') + 1);

                                if(Suratebi==null)Suratebi=new String[]{"","","","","","","","","",""};
                                Suratebi[Integer.parseInt(tag)]=url;
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }/////////////////////////// suratis gadagebis funqcia

        if (requestCode == BARCODE_SCANNER) {  /////////////////////////// QR code scanirebis funqcia
            if (resultCode == RESULT_OK) {

                String contents = data.getStringExtra("SCAN_RESULT");
                String format = data.getStringExtra("SCAN_RESULT_FORMAT");

                //Pattern sPattern
                //        = Pattern.compile("(2005[1-9]*)");
                //Matcher m = sPattern.matcher(contents);
                //if(m.find())



                String KODI = contents
                        .replaceAll("\t", "")
                        .replaceAll("[^A-Za-z0-9]", "")
                        .replaceAll("\r", "")
                        .replaceAll("\n", "")
                        .replaceAll(" ", "")
                        .replaceAll("საქართველოს", "")
                        .replaceAll("სოფლის", "")
                        .replaceAll("მეურნეობის", "")
                        .replaceAll("სამეცნიერო", "")
                        .replaceAll("კვლევითი", "")
                        .replaceAll("ცენტრი", "")
                        .replaceAll("სსიპ", "")
                        .trim();


                KODI=KODI.replace("20150000", "");
                if(KODI.length()>6)
                {
                    KODI=KODI.replaceFirst("2015","");

                    if(KODI.startsWith("20")){
                        KODI=KODI.substring(5);
                    }
                }
                /*while(KODI.toString().startsWith("0") && KODI.toString().length()>1)
                    KODI=KODI.toString().substring(1);*/

                KODI=KODI.replaceFirst("^0+(?!$)","");

                unikaluriID=KODI;



                EditText et=(EditText)findViewById(R.id.Value_QRcode);
                et.setText(KODI);

                //Toast toast = Toast.makeText(this, "Content:" + contents + " Format:" + format, Toast.LENGTH_LONG);
                //toast.show();
            }
        } /////////////////////////// QR code scanirebis funqcia








    }




    public void Save(View v) {


        PTK_DatabaseHandler db =  new PTK_DatabaseHandler(this);
        PTK_row row=new PTK_row(PTKgetText(R.id.Value_QRcode),PTKgetText(R.id.Value_Dasaxeleba),
                PTKgetText(R.id.Value_Descr),
                "Location", "Category",
                PTKgetText(R.id.Value_Info),
                PTKgetText(R.id.Value_Raodenoba),
                ((Spinner) findViewById(R.id.Value_Ganzomileba)).getSelectedItem().toString(),
                PTKgetText(R.id.Value_MigebisTarigi),
                PTKgetText(R.id.Value_DamzadebisTarigi),
                PTKgetText(R.id.Value_GamokenebisVada)
        );
        if(Suratebi==null)Suratebi=new String[]{"","","","","","","","","",""};
        row.setSuratebi(Suratebi);
        if(sackisi_id=="")
       db.addChanaceri(
          row
        );
        else
        {
            row.setRealuriRaodenoba(row.getRaodenoba());
            row.setRaodenoba(row_sackisi.getRaodenoba());
            row.setShemocmebulia();
            row.setID(sackisi_id);
            db.updateChanaceri(row);
        }

/*
        String fname=unikaluriID+".txt";
        File f = new File(Environment.getExternalStorageDirectory(), fname);
        String string = createJSON();

        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(fname, this.MODE_PRIVATE);
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

 */       finish();


    }

    private String createJSON() {
        String s="";
        JSONObject json = new JSONObject();
        try {
            try {
                json.put("unikaluriID", unikaluriID);
                json.put("dasaxeleba", PTKgetText(R.id.Value_Dasaxeleba));
                json.put("info", PTKgetText(R.id.Value_Info));
                json.put("raodenoba", PTKgetText(R.id.Value_Raodenoba));
                json.put("QRcode", PTKgetText(R.id.Value_QRcode));
                json.put("gamokenebisvada", PTKgetText(R.id.Value_GamokenebisVada));
                json.put("damzadebistarigi", PTKgetText(R.id.Value_DamzadebisTarigi));
                json.put("migebistarigi", PTKgetText(R.id.Value_MigebisTarigi));
                json.put("ganzombileba", ((Spinner) findViewById(R.id.Value_Ganzomileba)).getSelectedItem().toString());
            }catch (Exception e){
                e.printStackTrace();
            }

            try {
                JSONObject jsonSuratebi = new JSONObject();
                for (int i = 0; i < 10; i++)
                    jsonSuratebi.put("" + (i + 1), Suratebi[i]);

                json.put("suratebi", jsonSuratebi);
            } catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception exx) {
            exx.printStackTrace();
        }
        s=json.toString();
        return s;
    }

    private String PTKgetText(int id) {
        EditText e = (EditText)findViewById(id);
        return  e.getText().toString();
    }

    public void Cancel(View v){
        finish();
    }



    public void onClicKategoriisArcheva(View V){
        FotosGadageba.PTKshowMessage("ბაზა ცარიელია", "კატეგორიების ბაზა ცარიელია", "", this);


    }

    public void onClicMdebareobisArcheva(View V){
        FotosGadageba.PTKshowMessage("ბაზა ცარიელია","ადგილმდებარეობების ბაზა ცარიელია", "", this);


    }

}
