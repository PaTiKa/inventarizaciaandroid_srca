package ge.itforce.pkoridze.inventarizacia;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Paata Koridze ( PKoridze@gmail.com ) on 05/11/2015.
 */
public class PTK_multilevel_object {
    private String _title;
    private List<PTK_multilevel_object> _children;
    private JSONObject JSON;


    public PTK_multilevel_object() {
        this._children = null;
        this._title = "";
    }

    public PTK_multilevel_object(String Title) {
        this._title = Title;
    }

    public PTK_multilevel_object(String Title, List<PTK_multilevel_object>  Children) {
        this(Title);
        this._children = Children;
    }

    public void setTitle(String Title) {
        this._title = Title;
    }

    public String getTitle(){
        return this._title;
    }

    public void addChildren(List<PTK_multilevel_object> children) {
        this._children=children;
    }

    public boolean hasChildren() {
        if(this._children == null) return false;
        return !this._children.isEmpty();
    }

    public String getJSONString() {
        JSONArray jsonArray = getJSON();
        return jsonArray.toString();
    }

    public JSONArray  getJSON() {


        JSONArray jsonArray= new JSONArray();
        jsonArray.put(this._children);

        return jsonArray;
    }

    public void addChild(PTK_multilevel_object Child){
        this._children.add(Child);
    }

    public int getChildrenRaodenoba(){
        if(this.hasChildren()) return this._children.size();
        else return 0;
    }




}
