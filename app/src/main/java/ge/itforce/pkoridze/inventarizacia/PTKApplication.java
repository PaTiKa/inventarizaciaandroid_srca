package ge.itforce.pkoridze.inventarizacia;

import android.app.Application;

/*
 * Created by PTK on 14.08.2016.
 */
public class PTKApplication extends Application {
    private String workingFolder="PTK";

    public String getWorkingFolder() {
        return workingFolder;
    }

    public void setWorkingFolder(String workingFolder) {
        this.workingFolder = workingFolder;
    }
}
