package ge.itforce.pkoridze.inventarizacia;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Console;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by paati on 14.08.2016.
 */

public class PTKArrayAdapter extends ArrayAdapter<PTK_row> {

    private int layout;
    private Context MainScreenContext;
    private PTK_DatabaseHandler db;
    private ArrayList<PTK_row> items;
    private ArrayList<PTK_row> sruli = new ArrayList<PTK_row>();
    private ItemFilterPTK filter;
    private final Object mLock = new Object();
    private  PTKArrayAdapter es=null;
    public boolean onlyqrcodesearch=false;

    public PTKArrayAdapter(Context context, int resource, ArrayList<PTK_row> objects, PTK_DatabaseHandler db) {
        super(context, resource, objects);
        MainScreenContext=context;
        this.layout = resource;
        this.db=db;
        this.items=objects;
        es=this;
        cloneItems(objects);
        MainScreen m=(MainScreen)context;
        final EditText srch = (EditText)m.findViewById(R.id.searchText);
        //srch.addTextChangedListener(searchWatcherPTK);
        srch.setOnEditorActionListener(new EditText.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                        actionId == EditorInfo.IME_ACTION_DONE ||
                        event.getAction() == KeyEvent.ACTION_DOWN &&
                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (!event.isShiftPressed()) {
                        // the user is done typing.

                        //Toast.makeText(getContext(), srch.getText(), Toast.LENGTH_SHORT).show();
                        findEverywhere();
                        String txt=srch.getText().toString();
                        es.getFilter().filter(txt);

                        return true; // consume.
                    }
                }
                return false;
            }
        });
    }

    protected void cloneItems(ArrayList<PTK_row> items) {
        for (Iterator iterator = items.iterator(); iterator
                .hasNext();) {
            PTK_row gi = (PTK_row) iterator.next();
            sruli.add(gi);
        }
    }

    protected void cloneItems(List<PTK_row> items) {
        for (Iterator iterator = items.iterator(); iterator
                .hasNext();) {
            PTK_row gi = (PTK_row) iterator.next();
            sruli.add(gi);
        }
    }

    @Override
    public int getCount() {
        int raod = 0;
        synchronized (mLock) {
            raod= items != null ? items.size() : 0;

        }

        return raod;
    }

    @Override
    public PTK_row getItem(int item) {
        PTK_row gi = null;
        synchronized(mLock) {
            gi = items!=null ? items.get(item) : null;

        }
        return gi;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderPTK_class mainViewHolderPTK=null;
        if(convertView == null){
            LayoutInflater inflater=LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);

            PTK_row row1=null;
            synchronized(mLock) {
                row1 = items.get(position);
            }
             PTK_row row=row1;
            final int position1=position;
            ////getItem(position);

            if(row !=null) {
                ViewHolderPTK_class ViewHolderPTK = new ViewHolderPTK_class();
                ViewHolderPTK._row=row;
                ViewHolderPTK._id = (TextView) convertView.findViewById(R.id.invetnary_id);
                ViewHolderPTK._name = (TextView) convertView.findViewById(R.id.inventary_name);
                ViewHolderPTK._qrcode = (TextView) convertView.findViewById(R.id.inventary_qrcode);
                ViewHolderPTK._raod = (TextView) convertView.findViewById(R.id.inventary_raod);
                ViewHolderPTK._location = (TextView) convertView.findViewById(R.id.AdgilmdebareobaInventaris);
                ViewHolderPTK._location.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                ViewHolderPTK._name.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                ViewHolderPTK._qrcode.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                ViewHolderPTK._raod.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                ViewHolderPTK._id.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });

                String _id = row.getID();
                String _qrcode = row.getQRcode();
                String _name = row.getName();
                String _raod = row.getRaodenoba();
                Boolean _shemocmebul = row.getShemocebulia();


                ViewHolderPTK._id.setText(_id);
                ViewHolderPTK._qrcode.setText(_qrcode);
                ViewHolderPTK._name.setText(_name);
                ViewHolderPTK._raod.setText(row.getRealuriRaodenoba() + " / " + _raod);
                ViewHolderPTK._location.setText(row.getLocation());
                if(row.getShemocebulia()) {
                    try {
                        String rrr1=_raod;
                        if(rrr1.contains("."))rrr1=rrr1.replace(",","");
                        rrr1=rrr1.replace(" ","");
                        rrr1=rrr1.replace(",",".");
                        String rrr2=row.getRealuriRaodenoba();
                        if(rrr2.contains("."))rrr2=rrr2.replace(",","");
                        rrr2=rrr2.replace(" ","");
                        rrr2=rrr2.replace(",",".");
                        double raodd = Double.parseDouble(rrr1);
                        double raodr = Double.parseDouble(rrr2);
                        if (raodd == raodr)
                            ViewHolderPTK._name.setBackgroundColor(Color.parseColor("#ffffff"));
                        else
                            ViewHolderPTK._name.setBackgroundColor(Color.parseColor("#ffffaa"));
                    }
                    catch (Exception ee)
                    {
                        Log.e("PTKarrayAdapter", ee.getMessage());
                        ee.printStackTrace();
                    }
                }
                else
                {

                    ViewHolderPTK._name.setBackgroundColor(Color.parseColor("#ff7777"));

                }


                convertView.setTag(ViewHolderPTK);
            }
        } else{
            mainViewHolderPTK=(ViewHolderPTK_class) convertView.getTag();

            PTK_row row=null;
            synchronized(mLock) {
                row = items.get(position);
            }

            ////getItem(position);

            if(row!=null) {

                String _id = row.getID();
                String _qrcode = row.getQRcode();
                String _name = row.getName();
                String _raod = row.getRaodenoba();
                String _location=row.getLocation();
                Boolean _shemocmebul = row.getShemocebulia();
                mainViewHolderPTK._row=row;
                mainViewHolderPTK._id.setText(_id);
                mainViewHolderPTK._qrcode.setText(_qrcode);
                mainViewHolderPTK._name.setText(_name);
                mainViewHolderPTK._raod.setText(row.getRealuriRaodenoba() + " / " + _raod);
                mainViewHolderPTK._location.setText(_location);

                if(row.getShemocebulia()) {
                    try {
                        String rrr1=_raod;
                        if(rrr1.contains("."))rrr1=rrr1.replace(",","");
                        rrr1=rrr1.replace(" ","");
                        rrr1=rrr1.replace(",",".");
                        String rrr2=row.getRealuriRaodenoba();
                        if(rrr2.contains("."))rrr2=rrr2.replace(",","");
                        rrr2=rrr2.replace(" ","");
                        rrr2=rrr2.replace(",",".");
                        double raodd = Double.parseDouble(rrr1);
                        double raodr = Double.parseDouble(rrr2);
                        if (raodd == raodr)
                            mainViewHolderPTK._name.setBackgroundColor(Color.parseColor("#ffffff"));
                        else
                            mainViewHolderPTK._name.setBackgroundColor(Color.parseColor("#ffffaa"));
                    }
                    catch (Exception ee)
                    {
                        Log.e("PTKarrayAdapter", ee.getMessage());
                        ee.printStackTrace();
                    }
                }
                else
                {

                    mainViewHolderPTK._name.setBackgroundColor(Color.parseColor("#ff7777"));

                }

                mainViewHolderPTK._location.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext());

                        // return false;
                    }
                });
                mainViewHolderPTK._name.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                mainViewHolderPTK._qrcode.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                mainViewHolderPTK._raod.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                mainViewHolderPTK._id.setOnClickListener(new PTKClickListenerNew_class(row) {
                    @Override
                    public void onClick(View v) {
                        int positio111 = getPosition(getCurrentRow());
                        String pp=String.valueOf(positio111);
                        String pp1=String.valueOf(position);
                        Log.d("onclick", "pp="+pp+ " pp1" +pp1);
                        ((MainScreen)MainScreenContext).showDialogPTKitem(getCurrentRow(), getContext(), positio111, position);

                        // return false;
                    }
                });
                convertView.setTag(mainViewHolderPTK);
            }
        }

        return convertView; //super.getView(position, convertView, parent);
    }

    private class PTKClickListenerNew_class implements View.OnClickListener {
        private PTK_row row;


        public PTKClickListenerNew_class(PTK_row row) {
            this.row = row;
        }

        public PTK_row getCurrentRow(){
            return this.row;
        }
        @Override
        public void onClick(View v) {

        }
    }


    private class ViewHolderPTK_class {
        PTK_row _row;
        TextView _id;
        TextView _qrcode;
        TextView _name;
        TextView _raod;
        TextView _location;

    }
    private void showDialogPTKitem(PTK_row row1, Context context, int positionCalc, int position) {
        final PTK_row row=row1;

        Log.v("PTK_DEBUG", " poscalc:"+ String.valueOf(positionCalc)+" pos: "+ String.valueOf(position));

        MainScreen x=(MainScreen)getContext();
        x.EditChanaceri(row.getID());
        return;

/*
        //Toast.makeText(getContext(), "daklika " + row.getID(), Toast.LENGTH_SHORT).show();
        // getContext().startActivity(new Intent(MainSc    reen, Show.class));
        try {
            final Dialog d = new Dialog(context);
            d.setContentView(R.layout.chanaceri_popup);
            d.setCancelable(false);
            Button OKButton = (Button) d.findViewById(R.id.chanaceri_accepted);
            OKButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.updateRealuriRaodenobaAndAccept(row.getID(), ((EditText) d.findViewById(R.id.chanaceri_realuriraodenoba)).getText().toString());
                    d.dismiss();
                }
            });
            Button CancelButton = (Button) d.findViewById(R.id.chanaceri_accepted);
            CancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.dismiss();
                }
            });
            String[] surz = row.getSuratebi();

            if (surz !=null && surz.length > 0)
                ((ImageView) d.findViewById(R.id.chanaceri_image)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory() + File.separator + "PTK" + File.separator + surz[0])));
            ((TextView) d.findViewById(R.id.chanaceri_id)).setText(row.getID());
            ((TextView) d.findViewById(R.id.chanaceri_kodi)).setText(row.getQRcode());
            ((TextView) d.findViewById(R.id.chanaceri_name)).setText(row.getName());
            ((TextView) d.findViewById(R.id.chanaceri_raod)).setText(row.getRaodenoba());
            ((EditText) d.findViewById(R.id.chanaceri_realuriraodenoba)).setText(row.getRealuriRaodenoba());

            d.show();
        }
        catch (Exception exx)
        {
            exx.printStackTrace();
        }
        */
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new ItemFilterPTK();
        }
        return filter;
    }

    public void findOnlyQRCodes()
    {
        this.onlyqrcodesearch=true;
    }

    public boolean isOnlyQrCodeSearchable()
    {
        return this.onlyqrcodesearch;
    }
    public void findEverywhere()
    {
        this.onlyqrcodesearch=false;
    }

    private class ItemFilterPTK extends Filter {
        public ItemFilterPTK() {
            super();
        }

        protected FilterResults performFiltering(CharSequence prefix) {
            // Initiate our results object
            FilterResults results = new FilterResults();

            // No prefix is sent to filter by so we're going to send back the original array
            if (prefix == null || prefix.length() == 0) {
                synchronized (mLock) {
                    results.values = sruli;
                    results.count = sruli.size();
                }
            } else {
                synchronized(mLock) {
                    // Compare lower case strings
                    String prefixString = prefix.toString().toLowerCase();
                    final ArrayList<PTK_row> filteredItems = new ArrayList<PTK_row>();
                    // Local to here so we're not changing actual array
                    final ArrayList<PTK_row> localItems = new ArrayList<PTK_row>();
                    localItems.addAll(sruli);
                    final int count = localItems.size();

                    for (int i = 0; i < count; i++) {
                        final PTK_row item = localItems.get(i);
                        final String itemName = item.getName().toString().toLowerCase();
                        final String itemId = item.getID().toString().toLowerCase();
                        final String descr = item.getDescription().toString().toLowerCase();
                        final String qrcode = item.getQRcode().toString().toLowerCase();


                        if(onlyqrcodesearch) {
                            if (qrcode.equals(prefixString)) {
                                filteredItems.add(item);
                            }
                        }
                        else {
                                if (itemName.contains(prefixString) || itemId.contains(prefixString) || descr.contains(prefixString) || qrcode.contains(prefixString)) {
                                    filteredItems.add(item);
                                }
                            }

                    }

                    // Set and return
                    results.values = filteredItems;
                    results.count = filteredItems.size();
                }//end synchronized
            }

            return results;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence prefix, FilterResults results) {
            //noinspection unchecked
            synchronized(mLock) {
                final ArrayList<PTK_row> localItems = (ArrayList<PTK_row>) results.values;
                notifyDataSetChanged();
                clear();
                //Add the items back in
                for (Iterator iterator = localItems.iterator(); iterator
                        .hasNext();) {
                    PTK_row gi = (PTK_row) iterator.next();
                    add(gi);
                }
            }//end synchronized
        }
    }
    private final TextWatcher searchWatcherPTK = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //textView.setVisibility(View.VISIBLE);
        }

        public void afterTextChanged(Editable s) {
            Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
            /*if (s.length() == 0) {
                textView.setVisibility(View.GONE);
            } else{
                textView.setText("You have entered : " + passwordEditText.getText());
            }*/
        }
    };
}

