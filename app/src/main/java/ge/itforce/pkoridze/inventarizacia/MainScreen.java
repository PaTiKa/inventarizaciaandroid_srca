package ge.itforce.pkoridze.inventarizacia;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainScreen extends AppCompatActivity {

    int ADDNEW_RECORD = 15;
    int EDIT_RECORD = 30;
    int BARCODE_SCANNER =2;
    int CAPTURE_IMAGE= 0;

    String unikaluriID;
    private PTKArrayAdapter mainArrayAdapter=null;

    public ArrayList<String> ItemList= new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ((PTKApplication) this.getApplication()).setWorkingFolder("" + Environment.getExternalStorageDirectory() + File.separator + "PTK");

        File folder = new File("" + Environment.getExternalStorageDirectory() + File.separator + "PTK");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            File PTKfile = new File(folder.toString() + File.separator + "PTK.ptk");
            if (!PTKfile.exists()) {
                try {
                    PTKfile.createNewFile();
                } catch (Exception ex) {

                }
            }
            if (PTKfile.exists())
                MediaScannerConnection.scanFile(this, new String[]{PTKfile.toString()}, null, null);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Can't create folder")
                    .setMessage("Can't create working folder. Please manually create folder named PTK on SD card or in root folder (if SD card not installed)")
//                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // continue with delete
//                        }
//                    })
//                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // do nothing
//                        }
//                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }


        CreateDataBase();
        //TODO: gasarkvevia folderebis sheqmnis ambavi. ratomgac failis sheqmnis uflebas mazlevs mkholod root foldershi. danarchen folderebshi ara. folderis sheqmnis uflebas kidev arsad ar mazlevs
        //CreateFolders();
        LoadAndShowData();

        if (savedInstanceState != null) {
            EditText et = ((EditText)findViewById(R.id.searchText));
            String searchtxt=savedInstanceState.getString("searchString");
            et.setText(searchtxt);
            if(mainArrayAdapter!=null) {
                boolean onlyQRsearch=savedInstanceState.getBoolean("onlyQRsearch");

                if(onlyQRsearch)
                    mainArrayAdapter.findOnlyQRCodes();
                else
                    mainArrayAdapter.findEverywhere();
                mainArrayAdapter.getFilter().filter(searchtxt);

            }
        }


    }

    protected ArrayList<PTK_row>  convertListToArrayList(List<PTK_row> items) {
        ArrayList<PTK_row> shedegi=new ArrayList<PTK_row>();
        for (Iterator iterator = items.iterator(); iterator
                .hasNext();) {
            PTK_row gi = (PTK_row) iterator.next();
            shedegi.add(gi);
        }
        return shedegi;
    }


    private void LoadAndShowData() {
        try {
            PTK_DatabaseHandler db =  new PTK_DatabaseHandler(this);
            ListView lv =(ListView)findViewById(R.id.mainscreen_listview);
            mainArrayAdapter=new PTKArrayAdapter(this,R.layout.siiselementi_ptk_1,convertListToArrayList(db.getAllChanaceri()), db);
            lv.setAdapter(mainArrayAdapter);


            String foldername = ((PTKApplication) this.getApplication()).getWorkingFolder();
            for (File f:(new File(foldername)).listFiles()
                 ) {
                if(f.exists())
                    MediaScannerConnection.scanFile(this, new String[]{f.toString()},null,null);
            }


 /*            LinearLayout vv=(LinearLayout)findViewById(R.id.PTK_DataList);

           if(vv.getChildCount() > 0)
                vv.removeAllViews();
            //setContentView(vv);

            for (PTK_row row:db.getAllChanaceri()
                 ) {
                String id=row._Id;
                String ganzomileba=row.getGanzomileba();
                String dasaxeleba=row.getName();
                String description=row.getDescription();
                String info=row.getInfo();
                String raodneoba=row.getRaodenoba();
                String kategoria=row.getCategory();
                String adgilmdebareoba=row.getLocation();
                String damzadebisTarigi=row.getDamzadebisTarigi();
                String QRcode=row.getQRcode();
                //TODO: gamokenebis vada da migebis tarigis geteri aklia PTK_row -s
                //String gamokenebizVada=row.getGamokenebisVada();
                String[] suratebi = row.getSuratebi();
                String suratebiString=row.getSuratebiString();
                ItemList.add(QRcode+"\t"+ dasaxeleba);

                //FotosGadageba.PTKshowMessage("testi",dasaxeleba+" "+description+" "+info+" " +raodneoba+" "+ganzomileba,"", this);

                TextView textView = new TextView(this);
                //textView.setWidth(vv.getWidth()-5);
                textView.setPadding(0,8,0,12);
                //textView.getLayoutParams().width=vv.getLayoutParams().width-15;
                textView.setText(QRcode+" " + dasaxeleba);

                textView.setOnClickListener(new PTKClickListener(row));

                vv.addView(textView);

            }
            */
            //ArrayAdapter<String> adapter=new ArrayAdapter<String>(this, R.id.PTK_DataList, ItemList);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CreateFolders() {

        File folder = new File("" + Environment.getExternalStorageDirectory() + File.separator + "PTK");
        File filebi[] = folder.listFiles();// Environment.getExternalStorageDirectory().listFiles();

        File f = new File(folder, "PTK_CAPTURED_IMAGE.jpg"); new File(Environment.getExternalStorageDirectory(), "PTK_CAPTURED_IMAGE.jpg");
        try {
            FotosGadageba.PTKshowMessage("ცქმნი ფაილს", f.getAbsolutePath(), "", this);

            f.createNewFile();
            f.setWritable(true);

        } catch (Exception e) {
            FotosGadageba.PTKshowMessage(getString(R.string.FileVerSheiqmna), e.getMessage(), "", this);

        }

  //      String path = this.getFilesDir().getPath().toString();
  //      FotosGadageba.PTKshowMessage(path, path, "", this);

        String dir = "" + Environment.getExternalStorageDirectory() + File.separator + "PTK"; //Environment.getExternalStorageDirectory() + "/" + getString(R.string.WorkingFolder);//'"/inventarizacia/";
        String state = Environment.getExternalStorageState();

//        dir="/sdcard/"+ getString(R.string.WorkingFolder);
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state) || !Environment.MEDIA_MOUNTED.equals(state)) {
            FotosGadageba.PTKshowMessage("ERROR", "Error: external storage is read only or unavailable","", this);
        } else {
           // Log.d(LOG_TAG, "External storage is not read only or unavailable");
        }
        //final File folder =new File(Environment.getExternalStorageDirectory(), getString(R.string.WorkingFolder));//new File(dir); // + "/inventarizacia/");
        FotosGadageba.PTKshowMessage("ვქმნი ფოლდერს", folder.getAbsolutePath(), "", this);

        Boolean folderExists=false;
        if (!folder.exists()) {
            folderExists = folder.mkdirs();
        }
        else
            folderExists = true;

        if(!folderExists)
            FotosGadageba.PTKshowMessage(getString(R.string.WorkingFolderNotExists), dir, "", this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_close:
                System.exit(0);
                return true;
            case R.id.action_settings:
                new AlertDialog.Builder(this)
                        .setTitle("Working Folder")
                        .setMessage("" + Environment.getExternalStorageDirectory() + File.separator + "PTK")
//                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // continue with delete
//                        }
//                    })
//                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // do nothing
//                        }
//                    })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    //PTK
    public void onAxlisDamatebaClick(View v) {
        Intent myIntent = new Intent(this, AxaliChanacerisSheqmna.class);
        //myIntent.putExtra("key", value); //Optional parameters
        this.startActivityForResult(myIntent, ADDNEW_RECORD);
    }

    public void EditChanaceri(String id)
    {
        Intent myIntent = new Intent(this, AxaliChanacerisSheqmna.class);
        //myIntent.putExtra("key", value); //Optional parameters
        myIntent.putExtra("sackisi_id", id);
        this.startActivityForResult(myIntent, EDIT_RECORD);
    }



    //PTK
    public void CreateDataBase(){
        try {
            /*SQLiteDatabase myDB = null;

            myDB = this.openOrCreateDatabase("PTK_Inventory", MODE_PRIVATE, null);
            myDB.execSQL("CREATE TABLE IF NOT EXISTS " + " Mtavari " + " (_id integer PRIMARY KEY AUTOINCREMENT, name text NOT NULL, descr text NOT NULL, location text NOT NULL, category text NOT NULL, info text NOT NULL);");

            myDB.execSQL("insert into  Mtavari (_id, name, descr, location, category, info) VALUES(0,'blablabla','descr', 'sacxa', 'kategoria', 'damatebiti informacia'");

*/




        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        String serachText=((EditText)findViewById(R.id.searchText)).getText().toString();
        outState.putString("searchString", serachText);//putParcelable("imageCaptureUri", mImageCaptureUri);

        boolean qrsearch=false;
        if(mainArrayAdapter!=null) {
            qrsearch= mainArrayAdapter.isOnlyQrCodeSearchable();

        }
        outState.putBoolean("onlyQRsearch",qrsearch);
    }

    public void showDialogPTKitem(PTK_row row1, Context context) {
        showDialogPTKitem(row1, context, 0,0);
    }

    public void showDialogPTKitem(PTK_row row1, Context context, int positionCalc, int position) {
        final PTK_row row=row1;

        //Log.v("PTK_DEBUG", " poscalc:"+ String.valueOf(positionCalc)+" pos: "+ String.valueOf(position));

        //MainScreen x=(MainScreen)getContext();
        //x.
                EditChanaceri(row.getID());
        return;

/*
        //Toast.makeText(getContext(), "daklika " + row.getID(), Toast.LENGTH_SHORT).show();
        // getContext().startActivity(new Intent(MainSc    reen, Show.class));
        try {
            final Dialog d = new Dialog(context);
            d.setContentView(R.layout.chanaceri_popup);
            d.setCancelable(false);
            Button OKButton = (Button) d.findViewById(R.id.chanaceri_accepted);
            OKButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.updateRealuriRaodenobaAndAccept(row.getID(), ((EditText) d.findViewById(R.id.chanaceri_realuriraodenoba)).getText().toString());
                    d.dismiss();
                }
            });
            Button CancelButton = (Button) d.findViewById(R.id.chanaceri_accepted);
            CancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.dismiss();
                }
            });
            String[] surz = row.getSuratebi();

            if (surz !=null && surz.length > 0)
                ((ImageView) d.findViewById(R.id.chanaceri_image)).setImageURI(Uri.fromFile(new File("" + Environment.getExternalStorageDirectory() + File.separator + "PTK" + File.separator + surz[0])));
            ((TextView) d.findViewById(R.id.chanaceri_id)).setText(row.getID());
            ((TextView) d.findViewById(R.id.chanaceri_kodi)).setText(row.getQRcode());
            ((TextView) d.findViewById(R.id.chanaceri_name)).setText(row.getName());
            ((TextView) d.findViewById(R.id.chanaceri_raod)).setText(row.getRaodenoba());
            ((EditText) d.findViewById(R.id.chanaceri_realuriraodenoba)).setText(row.getRealuriRaodenoba());

            d.show();
        }
        catch (Exception exx)
        {
            exx.printStackTrace();
        }
        */
    }




    /////////////////////////// suratis gadagebis funqcia
    //////////////////<----------------------------------

    //////////////////--------------------------->
    /////////////////////////// QR code scanirebis funqcia


    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    //product barcode mode
    public void scanBar(View v) {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            startActivityForResult(intent, BARCODE_SCANNER);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    //product qr code mode
    public void scanQR(View v) {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, BARCODE_SCANNER);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == BARCODE_SCANNER) {  /////////////////////////// QR code scanirebis funqcia
            if (resultCode == RESULT_OK) {

                String contents = data.getStringExtra("SCAN_RESULT");
                String format = data.getStringExtra("SCAN_RESULT_FORMAT");

                //Pattern sPattern
                //        = Pattern.compile("(2005[1-9]*)");
                //Matcher m = sPattern.matcher(contents);
                //if(m.find())



                String KODI = contents
                        .trim()
                        .replaceAll("[^A-Za-z0-9]", "")
                        .replaceAll("\t", "")
                        .replaceAll("\r", "")
                        .replaceAll("\n", "")
                        .replaceAll(" ", "")
                        .replaceAll("საქართველოს", "")
                        .replaceAll("სოფლის", "")
                        .replaceAll("მეურნეობის", "")
                        .replaceAll("სამეცნიერო", "")
                        .replaceAll("კვლევითი", "")
                        .replaceAll("ცენტრი", "")
                        .replaceAll("სსიპ", "")
                        .trim();

                KODI=KODI.replace("20150000", "");
                if(KODI.length()>6)
                {
                    KODI=KODI.replaceFirst("2015","");
                    if(KODI.startsWith("20")){
                        KODI=KODI.substring(5);
                    }
                }
                /*while(KODI.toString().startsWith("0") && KODI.toString().length()>1)
                    KODI=KODI.toString().substring(1);*/

                KODI=KODI.replaceFirst("^0+(?!$)","");

                unikaluriID=KODI;
                EditText et=(EditText)findViewById(R.id.searchText);
                et.setText(KODI);
                if(mainArrayAdapter!=null) {
                    mainArrayAdapter.findOnlyQRCodes();
                    mainArrayAdapter.getFilter().filter(KODI);

                }

                //Toast toast = Toast.makeText(this, "Content:" + contents + " Format:" + format, Toast.LENGTH_LONG);
                //toast.show();
            }
        } /////////////////////////// QR code scanirebis funqcia

        if (requestCode == ADDNEW_RECORD) {  /////////////////////////// daemata akhali chanceri
            if (resultCode == RESULT_OK) {


            }
            LoadAndShowData();
        } /////////////////////////// daemata akhali chanceri



        if (requestCode == EDIT_RECORD) {  /////////////////////////// daredaqtirda chanceri
            if (resultCode == RESULT_OK) {


            }
            LoadAndShowData();
        } /////////////////////////// daredaqtirda chanceri




    }


    /////////////////////////// QR code scanirebis funqcia
    //////////////////<----------------------------------


    //alert dialog for downloadDialog
    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }


    public void SynchronizeByWIFI(View V) {
        try {
            //tODO: gasatestia da tu mushaobs mashin amis mikhedvitaaa gasaketebeli
            Socket socket = new Socket("192.168.0.102",11000);
            DataOutputStream DOS = new DataOutputStream(socket.getOutputStream());
            DOS.writeUTF("HELLO_WORLD");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onFiltrisGasuftaveba(View view) {
        ((EditText)findViewById(R.id.searchText)).setText("");
        if(mainArrayAdapter!=null) {
            mainArrayAdapter.findEverywhere();
            mainArrayAdapter.getFilter().filter("");

        }

    }
}
