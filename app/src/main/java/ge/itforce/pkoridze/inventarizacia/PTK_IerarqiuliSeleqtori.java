package ge.itforce.pkoridze.inventarizacia;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

public class PTK_IerarqiuliSeleqtori extends AppCompatActivity {

    List<PTK_multilevel_object> _content = null;

    public void PTK_multilevel_object() {
        setContent();
    }

    public void PTK_multilevel_object(List<PTK_multilevel_object> Content) {
        this._content = Content;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ptk__ierarqiuli_seleqtori);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

    public void setContent(List<PTK_multilevel_object> Content) {
        this._content = Content;
    }


    public void setContent(){
        PTK_multilevel_object obj=new PTK_multilevel_object("Kategoria 1");
        obj.addChild(new PTK_multilevel_object("Qvekategoria 1"));
        obj.addChild(new PTK_multilevel_object("Qvekategoria 2"));

        PTK_multilevel_object obj1=new PTK_multilevel_object("Qvekategoria 3");
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 3 1"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 3 2"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 3 3"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 3 4"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 3 5"));
        obj.addChild(obj1);

        obj1 = new PTK_multilevel_object("Qvekategoria 4");
        PTK_multilevel_object obj2 = new PTK_multilevel_object("Qvekategoria 4 1");
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 4 1 1"));
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 4 1 2"));
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 4 1 3"));
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 4 1 4"));

        obj1.addChild(new PTK_multilevel_object("Qvekategoria 4 2"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 4 3"));

        this._content.add(obj);


        obj=new PTK_multilevel_object("Kategoria 2");
        obj.addChild(new PTK_multilevel_object("Qvekategoria 5"));
        obj.addChild(new PTK_multilevel_object("Qvekategoria 6"));
        obj.addChild(new PTK_multilevel_object("Qvekategoria 8"));

        obj1=new PTK_multilevel_object("Qvekategoria 8");
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 8 1"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 8 2"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 8 3"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 8 4"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 8 5"));
        obj.addChild(obj1);

        obj1 = new PTK_multilevel_object("Qvekategoria 9");
        obj2 = new PTK_multilevel_object("Qvekategoria 9 1");
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 9 1 1"));
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 9 1 2"));
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 9 1 3"));
        obj2.addChild(new PTK_multilevel_object("Qvekategoria 9 1 4"));

        obj1.addChild(new PTK_multilevel_object("Qvekategoria 9 2"));
        obj1.addChild(new PTK_multilevel_object("Qvekategoria 9 3"));

        this._content.add(obj);
    }

}