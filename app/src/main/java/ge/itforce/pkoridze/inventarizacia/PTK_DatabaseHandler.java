package ge.itforce.pkoridze.inventarizacia;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paata Koridze ( PKoridze@gmail.com ) 04/11/2015.
 */
public class PTK_DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "PTK_inventarizacia";

    private static final String DATABASE_FILE_PATH  = "" + Environment.getExternalStorageDirectory()+File.separator+"PTK";



    // Contacts table name
    private static final String TABLE_MTAVARI = "Mtavari";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "Dasaxeleba";
    private static final String KEY_DESCR = "Description";
    private static final String KEY_LOCATION = "Mdebareoba";
    private static final String KEY_LOCATION_ID = "Mdebareoba_id";
    private static final String KEY_INFO = "Info";
    private static final String KEY_SHEMOCMEBULI = "Shemocmebulia";
    private static final String KEY_QRcode = "QRcode";
    private static final String KEY_SURATEBI = "suratebi";
    private static final String KEY_CATEGORY = "Kategoria";
    private static final String KEY_CATEGORY_ID = "Kategoria_id";
    private static final String KEY_RAODENOBA = "Raodenoba";
    private static final String KEY_RealuriRAODENOBA = "RealuriRaodenoba";
    private static final String KEY_GANZOMILEBA = "Ganzomileba";
    private static final String KEY_GANZOMILEBAID = "Ganzomileba_id";
    private static final String KEY_MIGEBISTARIGI="MigebisTarigi";
    private static final String KEY_DamzadebisTarigi="DamzadebisTarigi";
    private static final String KEY_GamokenebisVada="GamokenebisVada";
    private static final String KEY_UndaRaodenoba="UndaRaodenoba";
    private static final String KEY_UndaGanzomileba="UndaGanzomileba";
    private static final String KEY_UndaDamzadebisTarigi="UndaDamzadebisTarigi";
    private static final String KEY_UndaGamokenebisVada="UndaGamokenebisVada";

    public PTK_DatabaseHandler(Context context) {

        super(context,DATABASE_FILE_PATH + File.separator + DATABASE_NAME, null, DATABASE_VERSION);

        File folder = new File(DATABASE_FILE_PATH);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            File databasefile=new File(folder.toString()+File.separator+DATABASE_NAME);
            if(databasefile.exists())
                MediaScannerConnection.scanFile(context, new String[]{databasefile.toString()},null,null);
        } else {
            new AlertDialog.Builder(context)
                    .setTitle("Can't create folder")
                    .setMessage("Can't create working folder. Please manually create folder named PTK on SD card or in root folder (if SD card not installed)")
//                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // continue with delete
//                        }
//                    })
//                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // do nothing
//                        }
//                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MTAVARI + "("
                + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_QRcode + " TEXT, "
                + KEY_SHEMOCMEBULI + " TEXT, "
                + KEY_NAME + " TEXT, "
                + KEY_DESCR + " TEXT, "
                + KEY_CATEGORY + " TEXT, "
                + KEY_CATEGORY_ID + " TEXT, "
                + KEY_LOCATION + " TEXT, "
                + KEY_LOCATION_ID + " TEXT, "
                + KEY_INFO + " TEXT, "
                + KEY_RAODENOBA + " TEXT, "
                + KEY_RealuriRAODENOBA + " TEXT, "
                + KEY_GANZOMILEBA + " TEXT, "
                + KEY_GANZOMILEBAID + " TEXT, "
                + KEY_SURATEBI + " TEXT, "
                + KEY_MIGEBISTARIGI + " TEXT, "
                + KEY_DamzadebisTarigi + " TEXT, "
                + KEY_GamokenebisVada + " TEXT, "
                + KEY_UndaDamzadebisTarigi + " TEXT, "
                + KEY_UndaGamokenebisVada + " TEXT, "
                + KEY_UndaGanzomileba + " TEXT, "
                + KEY_UndaRaodenoba + " TEXT "
                + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

       /*// Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MTAVARI);

        // Create tables again
        onCreate(db);*/
    }

    public void addChanaceri(PTK_row row) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = createValues(row);

        // Inserting Row
        db.insert(TABLE_MTAVARI, null, values);
        db.close(); // Closing database connection
    }

    public int updateRealuriRaodenobaAndAccept(String id, String Raodenoba) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_RealuriRAODENOBA, Raodenoba);
        values.put(KEY_SHEMOCMEBULI, "1");

        // updating row
        return db.update(TABLE_MTAVARI, values, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
    }

    public ContentValues createValues(PTK_row row){
        ContentValues values = new ContentValues();
        values.put(KEY_QRcode, row.getQRcode());
        values.put(KEY_NAME, row.getName());
        values.put(KEY_DESCR, row.getDescription());
        values.put(KEY_CATEGORY, row.getCategory());
        values.put(KEY_LOCATION, row.getLocation());
        String shemocm=row.getShemocebulia()?"1":"0";
        values.put(KEY_SHEMOCMEBULI, shemocm);
        values.put(KEY_INFO, row.getInfo());
        values.put(KEY_SURATEBI, row.getSuratebiString());
        values.put(KEY_RAODENOBA, row.getRaodenoba());
        values.put(KEY_RealuriRAODENOBA, row.getRealuriRaodenoba());
        values.put(KEY_GANZOMILEBA, row.getGanzomileba());
        values.put(KEY_CATEGORY_ID, row.getCategoryID());
        values.put(KEY_LOCATION_ID, row.getLocationID());
        values.put(KEY_GANZOMILEBAID, row.getGanzomilebaID());
        values.put(KEY_MIGEBISTARIGI, row.getMigebisTarigi());
        values.put(KEY_DamzadebisTarigi, row.getDamzadebisTarigi());
        values.put(KEY_GamokenebisVada, row.getGamokenebisVada());
        values.put(KEY_UndaDamzadebisTarigi, row.getUndaDamzadebisTarigi());
        values.put(KEY_UndaGamokenebisVada, row.getUndaGamokenebisVada());
        values.put(KEY_UndaGanzomileba, row.getUndaGanzomileba());
        values.put(KEY_UndaRaodenoba, row.getUndaRaodenoba());



        return values;

    }

    public PTK_row getChanaceri(int id) {
        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.query(TABLE_MTAVARI, new String[] {
                        KEY_ID, KEY_QRcode,
                        KEY_NAME, KEY_DESCR, KEY_CATEGORY,
                        KEY_LOCATION, KEY_INFO, KEY_RAODENOBA, KEY_GANZOMILEBA, KEY_SURATEBI,
                        KEY_CATEGORY_ID, KEY_LOCATION_ID, KEY_GANZOMILEBAID,
                        KEY_MIGEBISTARIGI, KEY_DamzadebisTarigi, KEY_GamokenebisVada,
                        KEY_UndaDamzadebisTarigi, KEY_UndaGamokenebisVada, KEY_UndaGanzomileba, KEY_UndaRaodenoba,
                KEY_RealuriRAODENOBA, KEY_SHEMOCMEBULI
                }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        PTK_row row = new PTK_row(cursor);
        // return contact
        return row;
    }

    public List<PTK_row> getAllChanaceri() {
        List<PTK_row> contactList = new ArrayList<PTK_row>();
        // Select All Query

        String selectQuery = "SELECT  "+
                KEY_ID+", "+
                KEY_QRcode+", "+
                KEY_NAME+", "+
                KEY_DESCR+", "+
                KEY_CATEGORY+", "+
                KEY_LOCATION+", "+
                KEY_INFO+", "+
                KEY_RAODENOBA+", "+
                KEY_GANZOMILEBA+", "+
                KEY_SURATEBI+", "+
                KEY_CATEGORY_ID+", "+
                KEY_LOCATION_ID+", "+
                KEY_GANZOMILEBAID+", "+
                KEY_MIGEBISTARIGI+", "+
                KEY_DamzadebisTarigi+", "+
                KEY_GamokenebisVada+", "+
                KEY_UndaDamzadebisTarigi+", "+
                KEY_UndaGamokenebisVada+", "+
                KEY_UndaGanzomileba+", "+
                KEY_UndaRaodenoba+", "+
                KEY_RealuriRAODENOBA+", "+
                KEY_SHEMOCMEBULI+" "+
                " FROM " + TABLE_MTAVARI;

        SQLiteDatabase db = this.getReadableDatabase();//PTK:   es iko   ->   this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[0]);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                try {
                    PTK_row row = new PTK_row(cursor);
                    contactList.add(row);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }


        // return contact list
        return contactList;
    }

    public int getChanaceriCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MTAVARI;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public int updateChanaceri(PTK_row row) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = createValues(row);


        // updating row
        return db.update(TABLE_MTAVARI, values, KEY_ID + " = ?",
                new String[]{String.valueOf(row.getID())});
    }

    public void deleteChanaceri(PTK_row row) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MTAVARI, KEY_ID + " = ?",
                new String[] { String.valueOf(row.getID()) });
        db.close();
    }


    public PTK_row getChanaceri(String sackisi_id) {
        int id=Integer.parseInt(sackisi_id);
        return  getChanaceri(id);
    }
}
