package ge.itforce.pkoridze.inventarizacia;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FotosGadageba extends AppCompatActivity {

    int TAKE_PHOTO_CODE = 0;


    private String dir = null, fname = "PTK_CAPTURED_IMAGE.jpg";



    private  boolean newImage=false;

    Uri outputFileUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotos_gadageba);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        if (savedInstanceState != null)
        {

            outputFileUri = (Uri)savedInstanceState.getParcelable("outputFileUri");
            dir = savedInstanceState.getString("dir");
            fname = savedInstanceState.getString("fname");
            newImage = savedInstanceState.getBoolean("newImage");
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putString("dir", dir);
        outState.putString("fname", fname);
        outState.putBoolean("newImage", newImage);
        outState.putParcelable("outputFileUri", outputFileUri);

    }

    public static void PTKshowMessage(String title, String message, final String moqmedeba, Context context){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (moqmedeba){
                            default:

                                break;
                        }
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    public void captureOnClick(View v){

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            fname = extras.getString("fname");
        }


        dir = "" + Environment.getExternalStorageDirectory() + File.separator + "PTK" + File.separatorChar+fname;// ""+Environment.getExternalStorageDirectory()+ File.separatorChar+fname;
        File f = new File(dir);
        try {

            f.createNewFile();
            f.setWritable(true);

        } catch (Exception e) {
           // FotosGadageba.PTKshowMessage(getString(R.string.FileVerSheiqmna), e.getMessage(), "", this);

        }

        outputFileUri=Uri.fromFile(f);
        cameraIntent.putExtra( MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {

            ImageView iv= (ImageView)findViewById(R.id.PTK_image_preview);
            iv.setImageURI(outputFileUri);

            newImage = true;

        }
    }

    public void SaveOnClick(View v) {
        int idii=0;
        String tag="";
        Bundle extras = getIntent().getExtras();
        int ID_extradan=0;
        if(extras != null) {
            tag = extras.getString("v");
            ID_extradan = extras.getInt("id");
        }

        Intent myIntent = new Intent();
        if(outputFileUri==null){
            setResult(RESULT_CANCELED, myIntent);
        } else {
            File imgfile=new File(outputFileUri.toString());
            if(imgfile.exists())
                MediaScannerConnection.scanFile(getBaseContext(), new String[]{imgfile.toString()},null,null);

            String url = outputFileUri.toString();
            url=url.substring(url.lastIndexOf('/') + 1);
            File f=new File("" + Environment.getExternalStorageDirectory()+File.separator+"PTK"+File.separator+url);
            if(f.exists())
                MediaScannerConnection.scanFile(this, new String[]{f.toString()},null,null);
            myIntent.putExtra("PTK_pic", outputFileUri);
            myIntent.putExtra("PTK_newImage", newImage);
            myIntent.putExtra("PTK_delete", false);
            myIntent.putExtra("id", ID_extradan);
            myIntent.putExtra("tag", tag);
            myIntent.putExtra("PTK_fname", url);
            setResult(RESULT_OK, myIntent);
        }
        finish();
    }


    public void DeleteOnClick(View v) {
//TODO: suratis cashla unda gaketdes
        Intent myIntent = new Intent(this, FotosGadageba.class);
        myIntent.putExtra("v", (String) v.getTag());
        this.startActivity(myIntent);
    }

    public void CancelOnClick(View v) {

        Intent myIntent = new Intent(this, FotosGadageba.class);
        myIntent.putExtra("v", (String) v.getTag());
        this.startActivity(myIntent);
    }
}
