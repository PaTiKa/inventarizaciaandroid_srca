package ge.itforce.pkoridze.inventarizacia;

import android.view.View;

/*
 * Created by PTK on 11.08.2016.
 */

public class PTKClickListener implements View.OnClickListener
{

    PTK_row id;
    public PTKClickListener(PTK_row id) {
        this.id = id;
    }

    @Override
    public void onClick(View v)
    {
        AxaliChanacerisSheqmna chanaceri=new AxaliChanacerisSheqmna();
        chanaceri.setInitialData(id);
    }

}