package ge.itforce.pkoridze.inventarizacia;

import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Paata Koridze ( PKoridze@gmail.com ) 04/11/2015.
 */
public class PTK_row {
    String _Id = "0";
    String _Name;
    String _QRcode;
    String[] Suratebi;
    String _Descr;
    String _Location;
    String _Category;
    String _Info;
    String _Raodenoba;
    String _Ganzomileba;
    String _MigebisTarigi;
    String _GamokenebisVada;
    String _DamzadebisTarigi;
    String _categoryID;
    String _RealuriRaodenoba = "";
    boolean __Shemocmebul=false;

    Map<String,String> fields;
    String _LocationID;
    String _GanzomilebaID;
    String _UndaDamzadebisTarigi;
    String _UndaGamokenebisVada;
    String _UndaGanzomileba;
    String _UndaRaodenoba;


    public PTK_row(String _id, String QRcode, String Name, String Descr, String Location, String Category, String Info, String Raodenoba, String Ganzomileba, String MigebisTarigi, String DamzadebisTarigi, String GamokenebisVada){
        this( QRcode,  Name,  Descr,  Location,  Category,  Info, Raodenoba, Ganzomileba, MigebisTarigi, DamzadebisTarigi, GamokenebisVada);
        this._Id=_id;
    }

    public PTK_row(String QRcode, String Name, String Descr, String Location, String Category, String Info, String Raodenoba, String Ganzomileba, String MigebisTarigi, String DamzadebisTarigi, String GamokenebisVada){
        this(Name, Descr, Location, Category, Info, Raodenoba, Ganzomileba, MigebisTarigi, DamzadebisTarigi, GamokenebisVada);
        this.setQRcode(QRcode);
    }

    public PTK_row(String Name, String Descr, String Location, String Category, String Info, String Raodenoba, String Ganzomileba, String MigebisTarigi, String DamzadebisTarigi, String GamokenebisVada){
        this();
        this.setName(Name);
        this.setDescription(Descr);
        this.setLocation(Location);
        this.setCategory(Category);
        this.setInfo(Info);
        this.setRaodenoba(Raodenoba);
        this.setRealuriRaodenoba(Raodenoba);
        this.setGanzomileba(Ganzomileba);
        this.setDamzadebisTarigi(DamzadebisTarigi);
        this.setMigebisTarigi(MigebisTarigi);
        this.setGamokenebisVada(GamokenebisVada);

    }


    public PTK_row(){
        Suratebi=new String[]{"","","","","","","","","",""};
    }

    public PTK_row(Cursor cursor){

        this.setID(cursor.getString(0));
        this.setQRcode(cursor.getString(1));
        this.setName(cursor.getString(2));
        this.setDescription(cursor.getString(3));
        this.setCategory(cursor.getString(4));
        this.setLocation(cursor.getString(5));
        this.setInfo(cursor.getString(6));
        this.setRaodenoba(cursor.getString(7));
        this.setGanzomileba(cursor.getString(8));

        try {
            String surz=cursor.getString(9);
            if(surz!=null) {
                JSONArray jsonArray = new JSONArray(surz);
                String[] strArr = new String[jsonArray.length()];

                for (int i = 0; i < jsonArray.length(); i++) {
                    strArr[i] = jsonArray.getString(i);
                }

                this.setSuratebi(strArr);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception exx)
        {
            exx.printStackTrace();
        }

        this.setCategoryID(cursor.getString(10));
        this.setLocationID(cursor.getString(11));
        this.setGanzomilebaID(cursor.getString(12));

        this.setMigebisTarigi(cursor.getString(13));
        this.setDamzadebisTarigi(cursor.getString(14));
        this.setGamokenebisVada(cursor.getString(15));

        this.setUndaDamzadebisTarigi(cursor.getString(16));
        this.setUndaGamokenebisVada(cursor.getString(17));
        this.setUndaGanzomileba(cursor.getString(18));
        this.setUndaRaodenoba(cursor.getString(19));
        this.setRealuriRaodenoba(cursor.getString(20));
        this.setShemocmebulia(cursor.getInt(21)>0);

        /*KEY_ID, KEY_QRcode,
                KEY_NAME, KEY_DESCR, KEY_CATEGORY,
                KEY_LOCATION, KEY_INFO, KEY_RAODENOBA, KEY_GANZOMILEBA, KEY_SURATEBI,
                KEY_CATEGORY_ID, KEY_LOCATION_ID, KEY_GANZOMILEBAID,
                KEY_MIGEBISTARIGI, KEY_DamzadebisTarigi, KEY_GamokenebisVada,
                KEY_UndaDamzadebisTarigi, KEY_UndaGamokenebisVada, KEY_UndaGanzomileba, KEY_UndaRaodenoba*/


    }

    public void setShemocmebulia()
    {
        this.__Shemocmebul=true;


    }

    public void setShemocmebulia(boolean shemocmda)
    {
        this.__Shemocmebul=shemocmda;
    }

    public boolean getShemocebulia()
    {
        return __Shemocmebul;
    }

    public void setID(String _id){
        this._Id = _id;
    }

    public void setRealuriRaodenoba(String _RealuriRaodenoba){ this._RealuriRaodenoba=_RealuriRaodenoba; }

    public void setName(String Name){
        this._Name = Name;
    }

    public void setDescription (String Descr){
        this._Descr = Descr;
    }

    public void setLocation (String Location){
        this._Location = Location;
    }

    public void setCategory (String Category){
        this._Category = Category;
    }

    public void setInfo (String Info){
        this._Info = Info;
    }

    public void setQRcode (String QRcode){
        this._QRcode = QRcode;
    }

    public String getID(){
        return this._Id;
    }

    public String getQRcode(){
        return this._QRcode;
    }

    public String getName(){
        return this._Name;
    }

    public String getDescription (){
        return this._Descr;
    }

    public String getLocation (){
        return this._Location;
    }

    public String getCategory (){
        return this._Category;
    }

    public String getInfo (){
        return this._Info;
    }

    public void setSuratebi(String[] suratebi) {
        this.Suratebi = suratebi==null?(new String[]{"","","","","","","","","",""}):suratebi;
    }



    public String[] getSuratebi() {
        if(this.Suratebi==null)
            return new String[]{"","","","","","","","","",""};
        else
         return this.Suratebi;
    }

    public String getSuratebiString() {
        JSONArray jsonArray = new JSONArray();
        try {
            if (this.Suratebi != null) {
                for (int i = 0; i < 10; i++)
                    jsonArray.put(this.Suratebi[i]);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return jsonArray.toString();
    }

    public String getRaodenoba() {
        return this._Raodenoba;
    }

    public String getRealuriRaodenoba() {
        return this._RealuriRaodenoba;
    }

    public void setRaodenoba(String raodenoba) {
        this._Raodenoba = raodenoba;
    }

    public String getGanzomileba() {
        return this._Ganzomileba;
    }

    public void setGanzomileba(String Ganzomileba) {
        this._Ganzomileba = Ganzomileba;
    }

    public void setDamzadebisTarigi(String damzadebisTarigi) {
        this._DamzadebisTarigi = damzadebisTarigi;
    }

    public void setMigebisTarigi(String migebisTarigi) {
        this._MigebisTarigi = migebisTarigi;
    }

    public void setGamokenebisVada(String gamokenebisVada) {
        this._GamokenebisVada = gamokenebisVada;
    }

    public String getDamzadebisTarigi() {
        return this._DamzadebisTarigi;
    }

    public String getMigebisTarigi() {
        return this._MigebisTarigi;
    }

    public String getGamokenebisVada() {
        return this._GamokenebisVada;
    }

    public String getCategoryID() { return this._categoryID;     }

    public void setCategoryID(String categoryID) {   this._categoryID=categoryID;   }

    public String getLocationID() { return this._LocationID;     }

    public void setLocationID(String LocationID) {   this._LocationID=LocationID;   }

    public String getGanzomilebaID() { return this._GanzomilebaID;     }

    public void setGanzomilebaID(String GanzomilebaID) {   this._GanzomilebaID=GanzomilebaID;   }

    public String getUndaDamzadebisTarigi() { return this._UndaDamzadebisTarigi;     }

    public void setUndaDamzadebisTarigi(String UndaDamzadebisTarigi) {   this._UndaDamzadebisTarigi=UndaDamzadebisTarigi;   }

    public String getUndaGamokenebisVada() { return this._UndaGamokenebisVada;     }

    public void setUndaGamokenebisVada(String UndaGamokenebisVada) {   this._UndaGamokenebisVada=UndaGamokenebisVada;   }

    public String getUndaGanzomileba() { return this._UndaGanzomileba;     }

    public void setUndaGanzomileba(String UndaGanzomileba) {   this._UndaGanzomileba=UndaGanzomileba;   }

    public String getUndaRaodenoba() { return this._UndaRaodenoba;     }

    public void setUndaRaodenoba(String UndaRaodenoba) {   this._UndaRaodenoba=UndaRaodenoba;   }


}
